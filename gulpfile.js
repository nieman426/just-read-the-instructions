const gulp = require( 'gulp');
const babel = require( 'gulp-babel');
const  eslint = require('gulp-eslint');

gulp .task ('default' , function() {
// ESLint
    gulp.src(["es6/**/*.js","public/es6/**/*.js"])
        .pipe(eslint())
        .pipe(eslint.format());
    // Node source
// Get anything from below es6 dir with js extension.
// Result from Babel translation, will be stored in dist dir
    gulp .src ( "es6/**/*.js")
        . pipe( babel())
        . pipe( gulp. dest( "dist"));
// browser source
// Get anything from below public/es6 dir with js extension.
// Result from Babel translation, will be stored in public/dist dir
    gulp .src ( "public/es6/**/*.js")
        . pipe( babel())
        . pipe( gulp. dest( "public/dist"));
});

